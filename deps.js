// This file was autogenerated by ../closure/closure/bin/build/depswriter.py.
// Please do not edit.
goog.addDependency('../../../visualizer/car.js', ['monoid.Car'], ['goog.log']);
goog.addDependency('../../../visualizer/load_race_button.js', ['monoid.LoadRaceButton'], ['goog.dom.classlist', 'goog.fs', 'goog.fs.FileReader', 'goog.ui.Component', 'monoid.Race']);
goog.addDependency('../../../visualizer/main.js', ['monoid.main'], ['goog.debug.Console', 'goog.dom', 'monoid.LoadRaceButton', 'monoid.RaceView']);
goog.addDependency('../../../visualizer/race.js', ['monoid.Race'], ['goog.log', 'goog.log.Logger', 'monoid.Car', 'monoid.Track']);
goog.addDependency('../../../visualizer/race_chart.js', ['monoid.RaceChart'], ['goog.ui.Component']);
goog.addDependency('../../../visualizer/race_map.js', ['monoid.RaceMap'], ['goog.ui.Component']);
goog.addDependency('../../../visualizer/race_view.js', ['monoid.RaceView'], ['goog.dom.classlist', 'goog.events', 'goog.ui.Component', 'monoid.RaceChart', 'monoid.RaceMap']);
goog.addDependency('../../../visualizer/track.js', ['monoid.Track', 'monoid.TrackPiece'], []);
